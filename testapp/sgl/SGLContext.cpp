#include "sglContext.h"
#include <cfloat>

namespace sgl {
	Context::Context(int w, int h) :
		width(w),
		height(h),
		colorChannels(3),
		matrixMode(SGL_PROJECTION),
		drawMode(SGL_POINTS),
		fillMode(SGL_FILL),
		depthTest(false),
		drawing(false),
		sceneDef(false),
		areaLightDef(false),
		ptSize(1),
		currentAreaLight(nullptr),
		environmentMap(nullptr) {

		depthBuffer = new float[width * height];
		colorBuffer = new float[width * height * colorChannels];

		for (int i = 0; i < width * height; i++) depthBuffer[i] = FLT_MAX;

		projectionStack.push(mat4f(1.0f));
		modelviewStack.push(mat4f(1.0f));
		currentMatrix = &(projectionStack.top());
	}

	Context::~Context(void) {
		delete[] depthBuffer;
		delete[] colorBuffer;

		if (environmentMap != nullptr) {
			delete environmentMap;
		}

		currentMatrix = nullptr;
		this->clearScene();
	}

	void Context::clearScene(void) {
		for (auto primitive : primitives) delete primitive;
		primitives.clear();

		for (auto light : lights) delete light;
		lights.clear();
	}
}