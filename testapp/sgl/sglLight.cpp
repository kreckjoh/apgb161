#include "sglLight.h"
#include <algorithm>
#define _USE_MATH_DEFINES
#include <math.h>

namespace sgl {
	vec3f PointLight::lightLambert(const Material & material, const vec3f & toLight, const vec3f& normal) const {
		return material.color * this->color * material.kd * (std::max(dot(toLight, normal), 0.0f));
	}

	vec3f PointLight::lightPhong(const Material & material, const vec3f& surfacePos, const vec3f& cameraPos, const vec3f& normal) const {
		vec3f fragColor(0.0f);

		vec3f toCamera = normalize(cameraPos - surfacePos);
		vec3f toLight = normalize(this->pos - surfacePos);
		vec3f reflected = normalize(normal * 2 * dot(normal, toLight) - toLight);

		// diffuse
		fragColor += this->lightLambert(material, toLight, normal);

		// specular
		float cosCR = dot(toCamera, reflected);
		fragColor += this->color * material.ks * pow((std::max(cosCR, 0.0f)), material.shine);

		return fragColor;
	}

	vec3f PointLight::lightBlinnPhong(const Material & material, const vec3f& surfacePos, const vec3f& cameraPos, const vec3f& normal) const {
		vec3f fragColor(0.0f);

		vec3f toCamera = normalize(cameraPos - surfacePos);
		vec3f toLight = normalize(this->pos - surfacePos);
		vec3f halfVec = (toCamera + toLight) / length(toCamera + toLight);

		// diffuse
		fragColor += this->lightLambert(material, toLight, normal);

		// specular
		float cosHN = dot(halfVec, normal);
		fragColor += this->color * pow((std::max(cosHN, 0.0f)), material.shine) * material.ks;

		return fragColor;
	}

	vec3f PointLight::lightWard(const Material & material, const vec3f & surfacePos, const vec3f & cameraPos, const vec3f & normal) const {
		vec3f fragColor(0.0f);

		float alphaX = 0.2f;
		float alphaY = 0.5f;

		// create rotation matrix from a quaternion
		float angle(dot(vec3f(0.0f, 0.0f, 1.0f), normal));
		vec3f axis = cross(vec3f(0.0f, 0.0f, 1.0f), normal);
		mat4f planeTransform(vec4f(1 - 2 * axis.y * axis.y - 2 * axis.z * axis.z,
			2 * axis.x * axis.y + 2 * axis.z * angle,
			2 * axis.x * axis.z - 2 * axis.y * angle,
			0.0f),
			vec4f(2 * axis.x * axis.y - 2 * axis.z * angle,
				1 - 2 * axis.x * axis.x - 2 * axis.z * axis.z,
				2 * axis.x * axis.z + 2 * axis.x * angle,
				0.0f),
			vec4f(2 * axis.x * axis.z + 2 * axis.y * angle,
				2 * axis.y - axis.z - 2 * axis.x * angle,
				1 - 2 * axis.x * axis.x - 2 * axis.y * axis.y,
				0.0f),
			vec4f(0.0f, 0.0f, 0.0f, 1.0f));

		vec3f x = planeTransform * vec4f(1.0f, 0.0f, 0.0f, 1.0f);
		vec3f y = planeTransform * vec4f(0.0f, 1.0f, 0.0f, 1.0f);

		vec3f toCamera = normalize(cameraPos - surfacePos);
		vec3f toLight = normalize(this->pos - surfacePos);

		float cosLN = dot(normal, toLight);
		fragColor += this->lightLambert(material, toLight, normal);

		if (cosLN > 0.0) {
			vec3f halfVec = (toCamera + toLight) / length(toCamera + toLight);
			float cosCN = dot(toCamera, normal);
			float cosHN = dot(halfVec, normal);
			float cosHX = dot(halfVec, x);
			float cosHY = dot(halfVec, y);

			float exponent = -1.0f * (pow(cosHX / alphaX, 2) + pow(cosHY / alphaY, 2)) / pow(cosHN, 2);
			fragColor += this->color * material.ks / (4.0f * M_PI * alphaX * alphaY * sqrt(cosCN * cosLN)) * exp(exponent);
		}

		return fragColor;
	}

	AreaLight::~AreaLight(void) {
		for (auto triangle : triangles) delete triangle;
	}
}