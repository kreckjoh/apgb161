#include "sglMath.h"
#include <cmath>

template<typename T>
vec3<T> & vec3<T>::operator = (const vec3<T> & a) {
	if (this == &a) return *this;
	x = a.x;
	y = a.y;
	z = a.z;
	return *this;
}

template<typename T>
vec3<T> & vec3<T>::operator += (const vec3<T> & a) {
	x += a.x;
	y += a.y;
	z += a.z;
	return *this;
}

template<typename T>
vec3<T> vec3<T>::operator + (const vec3<T> & a) const {
	vec3<T> ret(*this);
	return ret += a;
}

template<typename T>
vec3<T> & vec3<T>::operator -= (const vec3<T>& a) {
	x -= a.x;
	y -= a.y;
	z -= a.z;
	return *this;
}

template<typename T>
vec3<T> vec3<T>::operator - (const vec3<T>& a) const {
	vec3<T> ret(*this);
	return ret -= a;
}

template<typename T>
vec3<T> & vec3<T>::operator /= (const T & a) {
	if (a != static_cast<T>(0)) {
		x /= a;
		y /= a;
		z /= a;
	}
	return *this;
}

template<typename T>
vec3<T> vec3<T>::operator / (const T & a) const {
	vec3<T> ret(*this);
	return ret /= a;
}

template<typename T>
vec3<T> & vec3<T>::operator *= (const T & a) {
	x *= a;
	y *= a;
	z *= a;
	return *this;
}

template<typename T>
vec3<T> vec3<T>::operator * (const T & a) const {
	vec3<T> ret(*this);
	return ret *= a;
}

template<typename T>
vec3<T>& vec3<T>::operator *= (const vec3<T>& a) {
	x *= a.x;
	y *= a.y;
	z *= a.z;
	return *this;
}

template<typename T>
vec3<T> vec3<T>::operator * (const vec3<T>& a) const {
	vec3<T> ret(*this);
	return ret *= a;
}

template<typename T>
vec3<T>& vec3<T>::operator /= (const vec3<T>& a) {
	x /= a.x;
	y /= a.y;
	z /= a.z;
	return *this;
}

template<typename T>
vec3<T> vec3<T>::operator / (const vec3<T>& a) const {
	vec3<T> ret(*this);
	return ret /= a;
}

template<typename T>
vec4<T> & vec4<T>::operator = (const vec4<T> & a) {
	if (this == &a) return *this;
	x = a.x;
	y = a.y;
	z = a.z;
	w = a.w;
	return *this;
}

template<typename T>
vec4<T> & vec4<T>::operator += (const vec4<T> & a) {
	x += a.x;
	y += a.y;
	z += a.z;
	w += a.w;
	return *this;
}

template<typename T>
vec4<T> vec4<T>::operator + (const vec4<T>& a) const {
	vec4<T> ret(*this);
	return ret += a;
}

template<typename T>
vec4<T>& vec4<T>::operator -= (const vec4<T>& a) {
	x -= a.x;
	y -= a.y;
	z -= a.z;
	w -= a.w;
	return *this;
}

template<typename T>
vec4<T> vec4<T>::operator - (const vec4<T>& a) const {
	vec4<T> ret(*this);
	return ret -= a;
}

template<typename T>
vec4<T> & vec4<T>::operator /= (const T & a) {
	if (a != static_cast<T>(0)) {
		x /= a;
		y /= a;
		z /= a;
		w /= a;
	}
	return *this;
}

template<typename T>
vec4<T> vec4<T>::operator / (const T & a) const {
	vec4<T> ret(*this);
	return ret /= a;
}

template<typename T>
vec4<T> & vec4<T>::operator *= (const T & a) {
	x *= a;
	y *= a;
	z *= a;
	w *= a;
	return *this;
}

template<typename T>
vec4<T> vec4<T>::operator * (const T & a) const {
	vec4<T> ret(*this);
	return ret *= a;
}

template<typename T>
std::ostream & operator << (std::ostream & out, const vec4<T> & a) {
	return out << a.x << " " << a.y << " " << a.z << " " << a.w;
}

template<typename T>
mat4<T>::mat4(const T & x) {
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++) {
			if ((i * 4 + j) % 5 == 0) data[i][j] = x;
			else data[i][j] = 0;
	}
}

template<typename T>
mat4<T>::mat4(const vec4<T> & a, const vec4<T> & b, const vec4<T> & c, const vec4<T> & d) {
	data[0][0] = a.x;
	data[0][1] = a.y;
	data[0][2] = a.z;
	data[0][3] = a.w;
	data[1][0] = b.x;
	data[1][1] = b.y;
	data[1][2] = b.z;
	data[1][3] = b.w;
	data[2][0] = c.x;
	data[2][1] = c.y;
	data[2][2] = c.z;
	data[2][3] = c.w;
	data[3][0] = d.x;
	data[3][1] = d.y;
	data[3][2] = d.z;
	data[3][3] = d.w;
}

template<typename T>
mat4<T>::mat4(const T * a) {
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			data[i][j] = a[i * 4 + j];
}

template<typename T>
mat4<T>::mat4(const mat4 & a) {
	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			data[i][j] = a.data[i][j];
}

template<typename T>
mat4<T> & mat4<T>::operator = (const mat4<T> & a) {
	if (this != &a) {
		for (int i = 0; i < 4; i++)
			for (int j = 0; j < 4; j++)
				data[i][j] = a.data[i][j];
	}
	return *this;
}

template<typename T>
mat4<T> & mat4<T>::operator *= (const mat4<T> & a) {
	mat4<T> ret;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			for (int r = 0; r < 4; r++) {
				ret.data[i][j] += this->data[r][j] * a.data[i][r];
			}
		}
	}
	*this = ret;
	return *this;
}

template<typename T>
mat4<T> mat4<T>::operator * (const mat4<T> & a) const {
	mat4<T> ret(*this);
	return ret *= a;
}

template<typename T>
std::ostream & operator << (std::ostream & out, const mat4<T> & a) {
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			out << a.data[j][i] << " ";
		}
		out << "\n";
	}
	return out;
}

template<typename T>
T dot(const vec3<T>& a, const vec3<T>& b) {
	return T(a.x * b.x + a.y * b.y + a.z * b.z);
}

template<typename T>
vec3<T> cross(const vec3<T>& a, const vec3<T>& b) {
	return vec3<T>(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x);
}

template<typename T>
float length(const vec3<T> & a) {
	return sqrt(a.x * a.x + a.y * a.y + a.z * a.z);
}

template<typename T>
vec3<T> normalize(vec3<T> a) {
	return a /= length(a);
}

template<typename T>
T clamp(const T & value, const T & low, const T & high) {
	if (value < low) return low;
	if (value > high) return high;
	return value;
}

template<typename T>
vec3<T> clamp(const vec3<T> & value, const T & low, const T & high) {
	vec3<T> ret;
	ret.x = clamp(value.x, low, high);
	ret.y = clamp(value.y, low, high);
	ret.z = clamp(value.z, low, high);
	return ret;
}

template<typename T>
vec4<T> operator * (const mat4<T> & mat, const vec4<T> & vec) {
	vec4<T> ret;
	ret.x = mat.data[0][0] * vec.x + mat.data[1][0] * vec.y + mat.data[2][0] * vec.z + mat.data[3][0] * vec.w;
	ret.y = mat.data[0][1] * vec.x + mat.data[1][1] * vec.y + mat.data[2][1] * vec.z + mat.data[3][1] * vec.w;
	ret.z = mat.data[0][2] * vec.x + mat.data[1][2] * vec.y + mat.data[2][2] * vec.z + mat.data[3][2] * vec.w;
	ret.w = mat.data[0][3] * vec.x + mat.data[1][3] * vec.y + mat.data[2][3] * vec.z + mat.data[3][3] * vec.w;
	return ret;
}

//template<typename T>
//vec3<T> operator * (const mat3<T>& mat, const vec3<T>& vec) {
//	vec4<T> ret;
//	ret.x = mat.data[0][0] * vec.x + mat.data[1][0] * vec.y + mat.data[2][0] * vec.z;
//	ret.y = mat.data[0][1] * vec.x + mat.data[1][1] * vec.y + mat.data[2][1] * vec.z;
//	ret.z = mat.data[0][2] * vec.x + mat.data[1][2] * vec.y + mat.data[2][2] * vec.z;
//	return ret;
//}

// inverse matrix computation using gauss_jacobi method 
// source: N.R. in C
// if matrix is regular = computatation successfull = returns 0
// in case of singular matrix returns 1
template<typename T>
int mat4<T>::invert(void) {
	int indxc[4], indxr[4], ipiv[4];
	int i, icol, irow, j, k, l, ll, n;
	T big, dum, pivinv, temp;
	// satisfy the compiler
	icol = irow = 0;

	// the size of the matrix
	n = 4;

	for (j = 0; j < n; j++) /* zero pivots */
		ipiv[j] = 0;

	for (i = 0; i < n; i++)
	{
		big = 0.0;
		for (j = 0; j < n; j++)
			if (ipiv[j] != 1)
				for (k = 0; k<n; k++)
				{
					if (ipiv[k] == 0)
					{
						if (fabs(data[k][j]) >= big)
						{
							big = fabs(data[k][j]);
							irow = j;
							icol = k;
						}
					}
					else
						if (ipiv[k] > 1)
							return 1; /* singular matrix */
				}
		++(ipiv[icol]);
		if (irow != icol)
		{
			for (l = 0; l<n; l++)
			{
				temp = data[l][icol];
				data[l][icol] = data[l][irow];
				data[l][irow] = temp;
			}
		}
		indxr[i] = irow;
		indxc[i] = icol;
		if (data[icol][icol] == 0.0)
			return 1; /* singular matrix */

		pivinv = 1.0 / data[icol][icol];
		data[icol][icol] = 1.0;
		for (l = 0; l<n; l++)
			data[l][icol] = data[l][icol] * pivinv;

		for (ll = 0; ll < n; ll++)
			if (ll != icol)
			{
				dum = data[icol][ll];
				data[icol][ll] = 0.0;
				for (l = 0; l<n; l++)
					data[l][ll] = data[l][ll] - data[l][icol] * dum;
			}
	}
	for (l = n; l--; )
	{
		if (indxr[l] != indxc[l])
			for (k = 0; k<n; k++)
			{
				temp = data[indxr[l]][k];
				data[indxr[l]][k] = data[indxc[l]][k];
				data[indxc[l]][k] = temp;
			}
	}

	return 0; // matrix is regular, inversion has been succesfull
}

template struct vec3<int>;
template struct vec3<float>;
template struct vec4<int>;
template struct vec4<float>;
template struct mat4<int>;
template struct mat4<float>;
template int dot(const vec3<int> & a, const vec3<int> & b);
template float dot(const vec3<float> & a, const vec3<float> & b);
template vec3<int> cross(const vec3<int> & a, const vec3<int> & b);
template vec3<float> cross(const vec3<float> & a, const vec3<float> & b);
template float length(const vec3<int> & a);
template float length(const vec3<float> & a);
template vec3<int> normalize(vec3<int> a);
template vec3<float> normalize(vec3<float> a);
template vec3<float> clamp(const vec3<float> & value, const float & low, const float & high);
template vec4<int> operator * (const mat4<int> & mat, const vec4<int> & vec);
template vec4<float> operator * (const mat4<float> & mat, const vec4<float> & vec);
//template vec3<int> operator * (const mat3<int> & mat, const vec3<int> & vec);
//template vec3<float> operator * (const mat3<float> & mat, const vec3<float> & vec);
template std::ostream & operator << (std::ostream & out, const mat4<int> & a);
template std::ostream & operator << (std::ostream & out, const mat4<float> & a);
template std::ostream & operator << (std::ostream & out, const vec4<int> & a);
template std::ostream & operator << (std::ostream & out, const vec4<float> & a);