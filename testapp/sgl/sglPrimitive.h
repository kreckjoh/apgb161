#ifndef __SGL_PRIMITIVE_H
#define __SGL_PRIMITIVE_H

#include "sglMath.h"
#include "sglMaterial.h"

namespace sgl {
	struct Ray {
		Ray(void) { }
		Ray(const vec3f & origin, const vec3f & direction) : origin(origin), direction(direction) { }

		vec3f origin, direction;
	};

	struct Primitive {
		Primitive(const Material & material) : material(material) { }
		virtual ~Primitive(void) { }

		virtual bool intersect(const Ray & ray, float & t) const = 0;
		virtual vec3f getNormal(const vec3f & surfacePos) const = 0;

		Material material;
	};

	struct Sphere : public Primitive {
		Sphere(const float & x, const float & y, const float & z, const float & r, const Material & material)
			: Primitive(material), center(vec3f(x, y, z)), r(r) { }

		bool intersect(const Ray & ray, float & dist) const;
		vec3f getNormal(const vec3f & surfacePos) const;

		vec3f center;
		float r;
	};

	struct Triangle : public Primitive {
		Triangle(const vec4f & a, const vec4f & b, const vec4f & c, const Material & material);

		bool intersect(const Ray & ray, float & t) const;
		vec3f getNormal(const vec3f & surfacePos) const;

		vec3f a, b, c, n;
		bool ignoreNormal;
	};
}

#endif // __SGL_PRIMITIVE_H