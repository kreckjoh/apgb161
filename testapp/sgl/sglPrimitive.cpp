#include "sglPrimitive.h"
#include <cmath>

namespace sgl {
	bool Sphere::intersect(const Ray & ray, float & t) const {
		vec3f toEye = ray.origin - center;
		float b = dot(toEye, ray.direction);
		float c = dot(toEye, toEye) - r * r;
		float d = b * b - c;

		if (d > 0.0f) {
			t = -b - sqrt(d);
			if (t < 0.01f) t = -b + sqrt(d);
			return true;
		}
		return false;
	}

	vec3f Sphere::getNormal(const vec3f& surfacePos) const {
		return normalize(surfacePos - center);
	}

	Triangle::Triangle(const vec4f & a, const vec4f & b, const vec4f & c, const Material & material)
		: Primitive(material),
		a(a),
		b(b),
		c(c) {
		n = normalize(cross(this->b - this->a, this->c - this->a));
	}

	bool Triangle::intersect(const Ray & ray, float & r) const {
		if (dot(ray.direction, n) > 0.0f) return false;
		
		vec3f u, v;  // triangle vectors
		vec3f w0, w; // ray vectors
		vec3f I;     // intersection
		float dot1, dot2;  // params to calc ray-plane intersect

		// get triangle edge vectors and plane normal
		u = b - a;
		v = c - a;

		w0 = ray.origin - a;
		dot1 = -dot(n, w0);
		dot2 = dot(n, ray.direction);
		if (fabs(dot2) < 0.01f) return false; // ray is  parallel to triangle plane

		// get intersect point of ray with triangle plane
		r = dot1 / dot2;
		if (r < 0.0f) return false; // ray goes away from triangle

		I = ray.origin + ray.direction * r; // intersect point of ray and plane

		// is I inside triangle?
		float uu, uv, vv, wu, wv, D;
		uu = dot(u, u);
		uv = dot(u, v);
		vv = dot(v, v);
		w = I - a;
		wu = dot(w, u);
		wv = dot(w, v);
		D = uv * uv - uu * vv;

		// get and test parametric coords
		float s, t;
		s = (uv * wv - vv * wu) / D;
		if (s < 0.0 || s > 1.0) return false; // I is outside T
		t = (uv * wu - uu * wv) / D;
		if (t < 0.0 || (s + t) > 1.0) return false; // I is outside T

		return true;
	}

	vec3f Triangle::getNormal(const vec3f& surfacePos) const {
		return n;
	}
}