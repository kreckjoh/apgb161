#ifndef __SGL_MATH_H
#define __SGL_MATH_H

#include <iostream>

template<typename T>
struct vec3;

template<typename T>
struct vec4 {
	vec4(const T & a) : x(a), y(a), z(a), w(a) { }
	vec4(void) : vec4(static_cast<T>(0)) { }
	vec4(const T & x, const T & y, const T & z, const T & w) : x(x), y(y), z(z), w(w) { }
	vec4(const vec4<T> & a) : x(a.x), y(a.y), z(a.z), w(a.w) { }
	vec4(const vec3<T> & a, const T & w) : x(a.x), y(a.y), z(a.z), w(w) { }

	vec4<T> & operator = (const vec4<T> & a);
	vec4<T> & operator += (const vec4<T> & a);
	vec4<T> operator + (const vec4<T> & a) const;
	vec4<T> & operator -= (const vec4<T> & a);
	vec4<T> operator - (const vec4<T> & a) const;
	vec4<T> & operator /= (const T & a);
	vec4<T> operator / (const T & a) const;
	vec4<T> & operator *= (const T & a);
	vec4<T> operator * (const T & a) const;

	T x, y, z, w;
};

typedef vec4<float> vec4f;
typedef vec4<int> vec4i;

template<typename T>
struct vec3 {
	vec3(const T & a) : x(a), y(a), z(a) { }
	vec3(void) : vec3(static_cast<T>(0)) { }
	vec3(const T & x, const T & y, const T & z) : x(x), y(y), z(z) { }
	vec3(const vec3<T> & a) : x(a.x), y(a.y), z(a.z) { }
	vec3(const vec4<T> & a) : x(a.x), y(a.y), z(a.z) { }

	vec3<T> & operator = (const vec3<T> & a);
	vec3<T> & operator += (const vec3<T> & a);
	vec3<T> operator + (const vec3<T> & a) const;
	vec3<T> & operator -= (const vec3<T> & a);
	vec3<T> operator - (const vec3<T> & a) const;
	vec3<T> & operator /= (const T & a);
	vec3<T> operator / (const T & a) const;
	vec3<T> & operator *= (const T & a);
	vec3<T> operator * (const T & a) const;
	vec3<T> & operator *= (const vec3<T> & a);
	vec3<T> operator * (const vec3<T> & a) const;
	vec3<T> & operator /= (const vec3<T> & a);
	vec3<T> operator / (const vec3<T> & a) const;
	
	T x, y, z;
};

typedef vec3<float> vec3f;
typedef vec3<int> vec3i;

template<typename T>
T dot(const vec3<T> & a, const vec3<T> & b);

template<typename T>
vec3<T> cross(const vec3<T> & a, const vec3<T> & b);

template<typename T>
float length(const vec3<T> & a);

template<typename T>
vec3<T> normalize(vec3<T> a);

template<typename T>
T clamp(const T & value, const T & low, const T & high);

template<typename T>
vec3<T> clamp(const vec3<T> & value, const T & low, const T & high);

template<typename T>
struct mat4 {
	mat4(const T & x);
	mat4(void) : mat4(static_cast<T>(0)) { }
	mat4(const vec4<T> & a, const vec4<T> & b, const vec4<T> & c, const vec4<T> & d); ///< Vectors form columns.
	mat4(const T * a); ///< No check for proper size!!!
	mat4(const mat4<T> & a);

	int invert(void);

	mat4<T> & operator = (const mat4<T> & a);
	mat4<T> & operator *= (const mat4<T> & a);
	mat4<T> operator * (const mat4<T> & a) const;

	T data[4][4]; ///< Saving data as columns.
};

typedef mat4<float> mat4f;
typedef mat4<int> mat4i;

template<typename T> vec4<T> operator * (const mat4<T> & mat, const vec4<T> & vec);
//template<typename T> vec3<T> operator * (const mat3<T> & mat, const vec3<T> & vec);
template<typename T> std::ostream & operator << (std::ostream & out, const mat4<T> & a);
template<typename T> std::ostream & operator << (std::ostream & out, const vec4<T> & a);

#endif // __SGL_MATH_H