#include "sglControl.h"

namespace sgl {
	Control::Control(void)
		: contextsSize(64),
		currentContext(nullptr),
		currentContextId(-1),
		activeContexts(0) {
		contexts = new Context *[contextsSize];
		for (int i = 0; i < contextsSize; i++) contexts[i] = nullptr;
	}

	Control::~Control(void) {
		for (int i = 0; i < contextsSize; i++)
			if (contexts[i] != nullptr) delete contexts[i];

		delete[] contexts;
	}
}