//---------------------------------------------------------------------------
// sgl.cpp
// Empty implementation of the SGL (Simple Graphics Library)
// Date:  2011/11/1
// Author: Jaroslav Krivanek, Jiri Bittner CTU Prague
//---------------------------------------------------------------------------

#include "sgl.h"
#include "sglContext.h"
#include "sglControl.h"

#include <iostream>
#include <new>
#include <cfloat>
#include <vector>
#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <set>
#include <typeinfo>
#include <cstdlib>
#include <ctime>

#define CHECK_CONTEXTS_DRAWING	if (sglcontrol->activeContexts <= 0 || sglcontrol->currentContext->drawing) {\
									setErrCode(SGL_INVALID_OPERATION);\
									return;\
								}
#define MAX_REC_LVL 8
#define FLT_EPS 0.01f
#define RAYS_PER_TRIANGLE 16

sgl::Control * sglcontrol = nullptr;

/// Current error code.
static sglEErrorCode _libStatus = SGL_NO_ERROR;

static inline void setErrCode(sglEErrorCode c) 
{
  if(_libStatus==SGL_NO_ERROR)
    _libStatus = c;
}

//---------------------------------------------------------------------------
// sglGetError()
//---------------------------------------------------------------------------
sglEErrorCode sglGetError(void) 
{
  sglEErrorCode ret = _libStatus;
  _libStatus = SGL_NO_ERROR;
  return ret;
}

//---------------------------------------------------------------------------
// sglGetErrorString()
//---------------------------------------------------------------------------
const char* sglGetErrorString(sglEErrorCode error)
{
  static const char *errStrigTable[] = 
  {
      "Operation succeeded",
      "Invalid argument(s) to a call",
      "Invalid enumeration argument(s) to a call",
      "Invalid call",
      "Quota of internal resources exceeded",
      "Internal library error",
      "Matrix stack overflow",
      "Matrix stack underflow",
      "Insufficient memory to finish the requested operation"
  };

  if((int)error<(int)SGL_NO_ERROR || (int)error>(int)SGL_OUT_OF_MEMORY ) {
    return "Invalid value passed to sglGetErrorString()"; 
  }

  return errStrigTable[(int)error];
}

//---------------------------------------------------------------------------
// Initialization functions
//---------------------------------------------------------------------------

void sglInit(void) {
	if (sglcontrol == nullptr) {
		try {
			sglcontrol = new sgl::Control();
		}
		catch (std::bad_alloc ex) {
			setErrCode(SGL_OUT_OF_MEMORY);
		}

		std::srand(std::time(NULL));
	}
	else std::cerr << "SGL already initialized!" << std::endl;
}

void sglFinish(void) {
	delete sglcontrol;
}

int sglCreateContext(int width, int height) {
	if (sglcontrol->activeContexts >= sglcontrol->contextsSize) {
		setErrCode(SGL_OUT_OF_RESOURCES);
		return -1;
	}

	sgl::Context * newContext;
	try {
		newContext = new sgl::Context(width, height);
	}
	catch (std::bad_alloc ex) {
		setErrCode(SGL_OUT_OF_MEMORY);
		return -1;
	}

	for (int i = 0; i < sglcontrol->contextsSize; i++) {
		if (sglcontrol->contexts[i] == nullptr) {
			sglcontrol->contexts[i] = newContext;
			sglcontrol->activeContexts++;
			return i;
		}
	}
	return -1;
}

void sglDestroyContext(int id) {
	if (sglcontrol->contexts[id] == nullptr) {
		setErrCode(SGL_INVALID_VALUE);
		return;
	}

	delete sglcontrol->contexts[id];
	sglcontrol->contexts[id] = nullptr;
	sglcontrol->activeContexts--;
}

void sglSetContext(int id) {
	if (id < 0 || id >= sglcontrol->contextsSize) {
		setErrCode(SGL_INVALID_VALUE);
		return;
	}
	if (sglcontrol->contexts[id] == nullptr) {
		setErrCode(SGL_INVALID_VALUE);
		return;
	}

	sglcontrol->currentContext = sglcontrol->contexts[id];
	sglcontrol->currentContextId = id;
}

int sglGetContext(void) {
	if (sglcontrol->activeContexts <= 0) {
		setErrCode(SGL_INVALID_OPERATION);
		return -1;
	}
	return sglcontrol->currentContextId;
}

float *sglGetColorBufferPointer(void) {
	if (sglcontrol->activeContexts <= 0) return 0;

	return sglcontrol->currentContext->colorBuffer;
}

//---------------------------------------------------------------------------
// Drawing functions
//---------------------------------------------------------------------------

void sglClearColor (float r, float g, float b, float alpha) {
	CHECK_CONTEXTS_DRAWING

	sglcontrol->currentContext->clearColor.x = r;
	sglcontrol->currentContext->clearColor.y = g;
	sglcontrol->currentContext->clearColor.z = b;
}

void sglClear(unsigned what) {
	CHECK_CONTEXTS_DRAWING

	sgl::Context * context = sglcontrol->currentContext;

	if (what & SGL_COLOR_BUFFER_BIT) {
		for (int i = 0; i < context->width * context->height * context->colorChannels; i += context->colorChannels) {
			context->colorBuffer[i+0] = context->clearColor.x;
			context->colorBuffer[i+1] = context->clearColor.y;
			context->colorBuffer[i+2] = context->clearColor.z;
		}
	}
	else if (what & SGL_DEPTH_BUFFER_BIT) {
		for (int i = 0; i < context->width * context->height; i++) context->depthBuffer[i] = FLT_MAX;
	}
	else {
		setErrCode(SGL_INVALID_VALUE);
		return;
	}
}

void sglBegin(sglEElementType mode) {
	if (mode >= SGL_LAST_ELEMENT_TYPE) {
		setErrCode(SGL_INVALID_ENUM);
		return;
	}
	if (sglcontrol->currentContext->drawing) {
		setErrCode(SGL_INVALID_OPERATION);
		return;
	}
	
	sglcontrol->currentContext->drawMode = mode;
	sglcontrol->currentContext->drawing = true;
}

void setColor(const int pos) {
	sgl::Context * context = sglcontrol->currentContext;
	context->colorBuffer[pos + 0] = context->color.x;
	context->colorBuffer[pos + 1] = context->color.y;
	context->colorBuffer[pos + 2] = context->color.z;
}

void setPixel(const int x, const int y, const float z) {
	sgl::Context * context = sglcontrol->currentContext;

	if (x >= context->width || x < 0 || y >= context->height || y < 0) return;

	int pos = x + y * context->width;

	if (sglcontrol->currentContext->depthTest) {
		if (context->depthBuffer[pos] <= z) return;
		context->depthBuffer[pos] = z;
	}

	pos *= context->colorChannels;
	setColor(pos);
}

void switchCoords(int & x1, int & y1, float & z1, int & x2, int & y2, float & z2) {
	int xtmp = x1;
	int ytmp = y1;
	float ztmp = z1;

	x1 = x2;
	y1 = y2;
	z1 = z2;
	
	x2 = xtmp;
	y2 = ytmp;
	z2 = ztmp;
}

void findIntersections(std::map<int, std::multiset<std::pair<int, float>>> & intersections, int x1, int y1, float z1, int x2, int y2, float z2) {
	if (y1 == y2) return;
	if (y1 < y2) switchCoords(x1, y1, z1, x2, y2, z2);

	float dx = -1.0f * static_cast<float>(x2 - x1) / static_cast<float>(y2 - y1);
	float dz = -1.0f * (z2 - z1) / (y2 - y1);

	for (int i = y1; i > y2; i--) {
		intersections[i].insert(std::pair<int, float>(x1 + static_cast<int>(round((y1 - i) * dx)), z1));
		z1 += dz;
	}
}

void lineHorizontal(int x1, float z1, int x2, float z2, int y) {
	sgl::Context * context = sglcontrol->currentContext;

	if (x1 < 0 || x2 < 0 || y >= context->height || y < 0) return;

	if (x1 > x2) switchCoords(x1, y, z1, x2, y, z2);

	if (x2 >= context->width) {
		z2 = x2 == x1 ? z2 : z1 + (context->width - 1 - x1) * (z2 - z1) / (x2 - x1);
		x2 = context->width - 1;
	}

	int posColor = x1 + y * context->width;
	int posDepth = posColor;
	posColor *= context->colorChannels;

	float dz = x2 == x1 ? 0 : (z2 - z1) / (x2 - x1);

	for (int i = x1; i <= x2; i++) {
		if (!sglcontrol->currentContext->depthTest)	setColor(posColor);
		else if (z1 < context->depthBuffer[posDepth]) {
			setColor(posColor);
			context->depthBuffer[posDepth] = z1;
		}

		posColor += context->colorChannels;
		posDepth++;
		z1 += dz;
	}
}

void fillScanline(void) {
	sgl::Context * context = sglcontrol->currentContext;
	std::map<int, std::multiset<std::pair<int, float>>> intersections;
	int x1, y1, x2, y2;
	float z1, z2;

	x1 = static_cast<int>(round(context->vertices.front().x));
	y1 = static_cast<int>(round(context->vertices.front().y));
	z1 = context->vertices.front().z;
	for (std::vector<vec4f>::iterator vertex = context->vertices.begin() + 1; vertex != context->vertices.end(); vertex++) {
		x2 = static_cast<int>(round(vertex->x));
		y2 = static_cast<int>(round(vertex->y));
		z2 = vertex->z;

		findIntersections(intersections, x1, y1, z1, x2, y2, z2);

		x1 = x2;
		y1 = y2;
		z1 = z2;
	}

	x1 = static_cast<int>(round(context->vertices.back().x));
	y1 = static_cast<int>(round(context->vertices.back().y));
	z1 = context->vertices.back().z;
	x2 = static_cast<int>(round(context->vertices.front().x));
	y2 = static_cast<int>(round(context->vertices.front().y));
	z2 = context->vertices.front().z;
	findIntersections(intersections, x1, y1, z1, x2, y2, z2);

	for (auto row : intersections) {
		for (std::multiset<std::pair<int, float>>::iterator intersection = row.second.begin(); intersection != row.second.end(); intersection++) {
			x1 = intersection->first;
			z1 = intersection->second;
			intersection++;
			x2 = intersection->first;
			z2 = intersection->second;

			lineHorizontal(x1, z1, x2, z2, row.first);
		}
	}
}

void lineBresenham(int x1, int y1, float z1, int x2, int y2, float z2) {
	int c0, c1, p;
	float zStep;

	int a = x2 - x1;
	int b = y2 - y1;
	
	if (a <= 0 && b <= 0) { // draw inversely both vertical and horizontal
		switchCoords(x1, y1, z1, x2, y2, z2);
		a = x2 - x1;
		b = y2 - y1;
	}

	if (abs(a) < abs(b)) { // vertical
		if (a > 0 && b < 0) {
			switchCoords(x1, y1, z1, x2, y2, z2);
			a = x2 - x1;
			b = y2 - y1;
		}

		zStep = (z2 - z1) / b;

		c0 = 2 * abs(a);
		c1 = c0 - 2 * abs(b);
		p = c0 - abs(b);

		int deltax;
		if (a > 0) deltax = 1; // doprava
		else deltax = -1; // doleva

		setPixel(x1, y1, z1);

		for (int i = y1 + 1; i <= y2; i++) {
			if (p > 0) {
				p += c1;
				x1 += deltax;
			}
			else {
				p += c0;
			}
			z1 += zStep;
			setPixel(x1, i, z1);
		}

	}
	else { // horizontal
		if (a < 0 && b > 0) {
			switchCoords(x1, y1, z1, x2, y2, z2);
			a = x2 - x1;
			b = y2 - y1;
		}

		zStep = (z2 - z1) / a;

		c0 = 2 * abs(b);
		c1 = c0 - 2 * abs(a);
		p = c0 - abs(a);
		
		int deltay;
		if (b > 0) deltay = 1; // up
		else deltay = -1; // down

		setPixel(x1, y1, z1);
		for (int i = x1 + 1; i <= x2; i++) {
			if (p > 0) {
				p += c1;
				y1 += deltay;
			}
			else {
				p += c0;
			}
			z1 += zStep;
			setPixel(i, y1, z1);
		}
	}
}

vec4f transformVertex(vec4f vertex) {
	sgl::Context * context = sglcontrol->currentContext;
	vec4f res = context->projectionStack.top() * context->modelviewStack.top() * vertex;
	res /= res.w;
	res = context->viewportMatrix * res;
	return res;
}

void drawPoints(void) {
	sgl::Context * context = sglcontrol->currentContext;
	for (auto vertex : context->vertices) {
		int x = static_cast<int>(round(vertex.x));
		int y = static_cast<int>(round(vertex.y));

		int offset;
		int xmin, xmax, ymin, ymax;

		if (context->ptSize % 2 == 1) {
			offset = (context->ptSize - 1) / 2;
			xmin = x - offset;
			ymin = y - offset;
			xmax = x + offset;
			ymax = y + offset;
		}
		else {
			offset = context->ptSize / 2;
			xmin = x - offset + 1;
			ymin = y - offset + 1;
			xmax = x + offset;
			ymax = y + offset;
		}

		if (xmin < 0) xmin = 0;
		if (ymin < 0) ymin = 0;
		while (xmax >= context->width) xmax--;
		while (ymax >= context->height) ymax--;

		for (int i = xmin; i <= xmax; i++)
			for (int j = ymin; j <= ymax; j++)
				setPixel(i, j, vertex.z);
	}
}

void drawLines(void) {
	sgl::Context * context = sglcontrol->currentContext;
	int x1, y1, x2, y2;
	float z1, z2;

	if (context->vertices.size() % 2 == 1) context->vertices.pop_back();

	for (std::vector<vec4f>::iterator current = context->vertices.begin(); current != context->vertices.end(); current += 2) {
		x1 = static_cast<int>(round(current->x));
		y1 = static_cast<int>(round(current->y));
		z1 = current->z;
		x2 = static_cast<int>(round((current + 1)->x));
		y2 = static_cast<int>(round((current + 1)->y));
		z2 = (current + 1)->z;

		lineBresenham(x1, y1, z1, x2, y2, z2);
	}
}

void drawLineStrip(void) {
	sgl::Context * context = sglcontrol->currentContext;
	int x1 = static_cast<int>(round(context->vertices.front().x));
	int y1 = static_cast<int>(round(context->vertices.front().y));
	float z1 = context->vertices.front().z;
	int x2, y2;
	float z2;

	for (auto current : context->vertices) {
		x2 = static_cast<int>(round(current.x));
		y2 = static_cast<int>(round(current.y));
		z2 = current.z;

		lineBresenham(x1, y1, z1, x2, y2, z2);

		x1 = x2;
		y1 = y2;
		z1 = z2;
	}
}

void drawLineLoop() {
	sgl::Context * context = sglcontrol->currentContext;
	drawLineStrip();

	int x1 = static_cast<int>(round(context->vertices.back().x));
	int y1 = static_cast<int>(round(context->vertices.back().y));
	float z1 = context->vertices.back().z;
	int x2 = static_cast<int>(round(context->vertices.front().x));
	int y2 = static_cast<int>(round(context->vertices.front().y));
	float z2 = context->vertices.front().z;

	lineBresenham(x1, y1, z1, x2, y2, z2);
}

template<typename T> // must be only sgl::Primitive or sgl::Triangle
void triangulate(std::list<T *> & mesh) {
	sgl::Context * context = sglcontrol->currentContext;

	if (context->vertices.size() < 3) {
		return;
	}

	for (auto vertex = context->vertices.begin() + 2; vertex != context->vertices.end(); ++vertex) {
		mesh.push_back(new sgl::Triangle(*(vertex - 2), *(vertex - 1), *vertex, context->material));
	}
}

void sglEnd(void) {
	if (!sglcontrol->currentContext->drawing) {
		setErrCode(SGL_INVALID_OPERATION);
		return;
	}
	sglcontrol->currentContext->drawing = false;

	sgl::Context * context = sglcontrol->currentContext;
	if (context->vertices.empty()) return;

	if (!context->sceneDef) {
		for (auto & it : context->vertices) it = transformVertex(it);
		switch (context->drawMode) {
			case SGL_POINTS: {
				drawPoints();
				break;
			}
			case SGL_LINES: {
				drawLines();
				break;
			}
			case SGL_LINE_STRIP: {
				drawLineStrip();
				break;
			}
			case SGL_LINE_LOOP: {
				drawLineLoop();
				break;
			}
			case SGL_TRIANGLES: {
				break;
			}
			case SGL_POLYGON: {
				if (context->fillMode == SGL_LINE) drawLineLoop();
				else if (context->fillMode == SGL_FILL) fillScanline();
				else drawPoints();
				break;
			}
			case SGL_AREA_LIGHT: {
				break;
			}
			default: {
				break;
			}
		}
	}
	else { // Scene definition
		if (context->areaLightDef) {
			std::list<sgl::Triangle * > mesh;
			triangulate(mesh);
			context->currentAreaLight->triangles.insert(context->currentAreaLight->triangles.end(), mesh.begin(), mesh.end());
		} else {
			std::list<sgl::Primitive * > mesh;
			triangulate(mesh);
			context->primitives.insert(context->primitives.end(), mesh.begin(), mesh.end());
		}
	}

	sglcontrol->currentContext->vertices.clear();
}

void sglVertex4f(float x, float y, float z, float w) {
	sglcontrol->currentContext->vertices.push_back(vec4f(x, y, z, w));
}

void sglVertex3f(float x, float y, float z) {
	sglcontrol->currentContext->vertices.push_back(vec4f(x, y, z, 1.0f));
}

void sglVertex2f(float x, float y) {
	sglcontrol->currentContext->vertices.push_back(vec4f(x, y, 0.0f, 1.0f));
}

void circularSymetry(const int x, const int y, const float z, const int xoffset, const int yoffset) {
	if (sglcontrol->currentContext->fillMode == SGL_LINE) {
		setPixel( x + xoffset,  y + yoffset, z);
		setPixel( y + xoffset,  x + yoffset, z);
		setPixel(-y + xoffset,  x + yoffset, z);
		setPixel(-x + xoffset,  y + yoffset, z);
		setPixel(-x + xoffset, -y + yoffset, z);
		setPixel(-y + xoffset, -x + yoffset, z);
		setPixel( y + xoffset, -x + yoffset, z);
		setPixel( x + xoffset, -y + yoffset, z);
	}
	else {
		lineHorizontal(-x + xoffset, z, x + xoffset, z,  y + yoffset);
		lineHorizontal(-y + xoffset, z, y + xoffset, z,  x + yoffset);
		lineHorizontal(-y + xoffset, z, y + xoffset, z, -x + yoffset);
		lineHorizontal(-x + xoffset, z, x + xoffset, z, -y + yoffset);
	}
}

void sglCircle(float x, float y, float z, float radius) {
	CHECK_CONTEXTS_DRAWING;
	if (radius <= 0.0f) {
		setErrCode(SGL_INVALID_VALUE);
		return;
	}
	
	vec4f center = transformVertex(vec4f(x, y, z, 1.0f));
	sgl::Context * context = sglcontrol->currentContext;

	if (context->fillMode == SGL_POINT) { // SGL_FILL or SGL_LINE resolved in circularSymetry()
		setPixel(static_cast<int>(round(center.x)), static_cast<int>(round(center.y)), center.z);
		return;
	}

	mat4f transformation = context->viewportMatrix * context->projectionStack.top() * context->modelviewStack.top();
	float det = transformation.data[0][0] * transformation.data[1][1] - transformation.data[0][1] * transformation.data[1][0];
	float scaleFactor = sqrt(det);
	radius *= scaleFactor;

	int dvex = 3;
	int dvey = static_cast<int>(round(2 * radius - 2));
	int p = static_cast<int>(round(1 - radius));
	int xn = 0;
	int yn = static_cast<int>(round(radius));
	int xoffset = static_cast<int>(round(center.x));
	int yoffset = static_cast<int>(round(center.y));

	while (xn <= yn) {
		circularSymetry(xn, yn, z, xoffset, yoffset);
		if (p > 0) {
			p -= dvey;
			dvey -= 2;
			yn -= 1;
		}
		p += dvex;
		dvex += 2;
		xn++;
	}
}

void sglEllipse(float x, float y, float z, float a, float b) {
	CHECK_CONTEXTS_DRAWING;
	if (a < 0.0f || b < 0.0f) {
		setErrCode(SGL_INVALID_VALUE);
		return;
	}

	if (sglcontrol->currentContext->fillMode == SGL_POINT) {
		sglBegin(SGL_POINTS);
		sglVertex3f(x, y, z);
		sglEnd();
		return;
	}

	int steps = 40;
	float step = 2 * M_PI / steps;
	float xn, yn;
	float t = 0;

	if (sglcontrol->currentContext->fillMode == SGL_LINE) sglBegin(SGL_LINE_LOOP);
	else sglBegin(SGL_POLYGON);
	for (int i = 0; i < steps; i++) {
		xn = a * cos(t);
		yn = b * sin(t);
		sglVertex3f(x + xn, y + yn, z);
		t += step;
	}
	sglEnd();
}

void sglArc(float x, float y, float z, float radius, float from, float to) {
	CHECK_CONTEXTS_DRAWING;

	if (sglcontrol->currentContext->fillMode == SGL_POINT) {
		sglBegin(SGL_POINTS);
		sglVertex3f(x, y, z);
		sglEnd();
		return;
	}
	
	int steps;
	float step;
	if (to > from) {
		steps = static_cast<int>(round(40 * (to - from) / (2 * M_PI)));
		step = (to - from) / steps;
	}
	else {
		steps = static_cast<int>(round(40 * (2 * M_PI - from + to) / (2 * M_PI)));
		step = (2 * M_PI - from + to) / steps;
	}
	
	float xn, yn;

	if (sglcontrol->currentContext->fillMode == SGL_LINE) sglBegin(SGL_LINE_STRIP);
	else {
		sglBegin(SGL_POLYGON);
		sglVertex3f(x, y, z);
	}
	for (int i = 0; i <= steps; i++) {
		xn = radius * cos(from);
		yn = radius * sin(from);
		sglVertex3f(x + xn, y + yn, z);
		from += step;
	}
	sglEnd();
}

//---------------------------------------------------------------------------
// Transform functions
//---------------------------------------------------------------------------

void sglMatrixMode(sglEMatrixMode mode) {
	CHECK_CONTEXTS_DRAWING;
	
	if (mode == SGL_MODELVIEW) {
		sglcontrol->currentContext->currentMatrix = &(sglcontrol->currentContext->modelviewStack.top());
		sglcontrol->currentContext->matrixMode = mode;
	}
	else if (mode == SGL_PROJECTION) {
		sglcontrol->currentContext->currentMatrix = &(sglcontrol->currentContext->projectionStack.top());
		sglcontrol->currentContext->matrixMode = mode;
	}
	else setErrCode(SGL_INVALID_ENUM);
}

void sglPushMatrix(void) {
	CHECK_CONTEXTS_DRAWING;

	mat4f matrix = *(sglcontrol->currentContext->currentMatrix);
	if (sglcontrol->currentContext->matrixMode == SGL_MODELVIEW) {
		if (sglcontrol->currentContext->modelviewStack.size() >= SGL_STACK_MAX_SIZE) {
			setErrCode(SGL_STACK_OVERFLOW);
			return;
		}
		sglcontrol->currentContext->modelviewStack.push(matrix);
		sglcontrol->currentContext->currentMatrix = &(sglcontrol->currentContext->modelviewStack.top());
	}
	else {
		if (sglcontrol->currentContext->projectionStack.size() >= SGL_STACK_MAX_SIZE) {
			setErrCode(SGL_STACK_OVERFLOW);
			return;
		}
		sglcontrol->currentContext->projectionStack.push(matrix);
		sglcontrol->currentContext->currentMatrix = &(sglcontrol->currentContext->projectionStack.top());
	}
}

void sglPopMatrix(void) {
	CHECK_CONTEXTS_DRAWING;

	if (sglcontrol->currentContext->matrixMode == SGL_MODELVIEW) {
		if (sglcontrol->currentContext->modelviewStack.size() <= 1) {
			setErrCode(SGL_STACK_UNDERFLOW);
			return;
		}
		sglcontrol->currentContext->modelviewStack.pop();
		sglcontrol->currentContext->currentMatrix = &(sglcontrol->currentContext->modelviewStack.top());
	}
	else {
		if (sglcontrol->currentContext->projectionStack.size() <= 1) {
			setErrCode(SGL_STACK_UNDERFLOW);
			return;
		}
		sglcontrol->currentContext->projectionStack.pop();
		sglcontrol->currentContext->currentMatrix = &(sglcontrol->currentContext->projectionStack.top());
	}
}

void sglLoadIdentity(void) {
	CHECK_CONTEXTS_DRAWING
	*(sglcontrol->currentContext->currentMatrix) = mat4f(1.0f);
}

void sglLoadMatrix(const float *matrix) {
	CHECK_CONTEXTS_DRAWING;
	*(sglcontrol->currentContext->currentMatrix) = *matrix; // deep copy!
}

void sglMultMatrix(const float *matrix) {
	CHECK_CONTEXTS_DRAWING;

	mat4f mult(matrix);
	*(sglcontrol->currentContext->currentMatrix) *= mult;
}

void sglTranslate(float x, float y, float z) {
	CHECK_CONTEXTS_DRAWING;
	
	mat4f translate = mat4f(vec4f(1.0f, 0.0f, 0.0f, 0.0f),
										vec4f(0.0f, 1.0f, 0.0f, 0.0f),
										vec4f(0.0f, 0.0f, 1.0f, 0.0f),
										vec4f(x, y, z, 1.0f));
	*(sglcontrol->currentContext->currentMatrix) *= translate;
}

void sglScale(float scalex, float scaley, float scalez) {
	CHECK_CONTEXTS_DRAWING;

	mat4f scale = mat4f(vec4f(scalex, 0.0f, 0.0f, 0.0f),
									vec4f(0.0f, scaley, 0.0f, 0.0f),
									vec4f(0.0f, 0.0f, scalez, 0.0f),
									vec4f(0.0f, 0.0f, 0.0f, 1.0f));
	*(sglcontrol->currentContext->currentMatrix) *= scale;
}

void sglRotate2D(float angle, float centerx, float centery) {
	CHECK_CONTEXTS_DRAWING;

	sglTranslate(centerx, centery, 0.0f);
	mat4f rotate = mat4f(vec4f(cos(angle), sin(angle), 0.0f, 0.0f),
									 vec4f(-sin(angle), cos(angle), 0.0f, 0.0f),
									 vec4f(0.0f, 0.0f, 1.0f, 0.0f),
									 vec4f(0.0f, 0.0f, 0.0f, 1.0f));
	*(sglcontrol->currentContext->currentMatrix) *= rotate;
	sglTranslate(-centerx, -centery, 0.0f);
}

void sglRotateY(float angle) {
	CHECK_CONTEXTS_DRAWING;

	mat4f rotate = mat4f(vec4f(cos(angle), 0.0f, sin(angle), 0.0f),
									 vec4f(0.0f, 1.0f, 0.0f, 0.0f),
									 vec4f(-sin(angle), 0.0f, cos(angle), 0.0f),
									 vec4f(0.0f, 0.0f, 0.0f, 1.0f));
	*(sglcontrol->currentContext->currentMatrix) *= rotate;
}

void sglOrtho(float left, float right, float bottom, float top, float near, float far) {
	CHECK_CONTEXTS_DRAWING;

	mat4f ortho(vec4f(2.0f / (right - left), 0.0f, 0.0f, 0.0f),
				      vec4f(0.0f, 2.0f / (top - bottom), 0.0f, 0.0f),
				      vec4f(0.0f, 0.0f, -2.0f / (far - near), 0.0f),
				      vec4f(-(right + left) / (right - left), -(top + bottom) / (top - bottom), -(far + near) / (far - near), 1));
	*(sglcontrol->currentContext->currentMatrix) *= ortho;
}

void sglFrustum(float left, float right, float bottom, float top, float near, float far) {
	CHECK_CONTEXTS_DRAWING;

	if (far <= 0.0f || near <= 0.0f) {
		setErrCode(SGL_INVALID_VALUE);
		return;
	}
	
	sglOrtho(left, right, bottom, top, near, far);
	mat4f frustum(vec4f(near, 0.0f, 0.0f, 0.0f),
					    vec4f(0.0f, near, 0.0f, 0.0f),
					    vec4f(0.0f, 0.0f, far + near, -1.0f),
					    vec4f(0.0f, 0.0f, far * near, 0.0f));
	*(sglcontrol->currentContext->currentMatrix) *= frustum;
}

void sglViewport(int x, int y, int width, int height) {
	CHECK_CONTEXTS_DRAWING;

	if (x < 0 || y < 0 || width < 0 || height < 0) {
		setErrCode(SGL_INVALID_VALUE);
		return;
	}

	sglcontrol->currentContext->viewportMatrix = mat4f(vec4f(width / 2.0f, 0.0f, 0.0f, 0.0f),
													   vec4f(0.0f, height / 2.0f, 0.0f, 0.0f),
													   vec4f(0.0f, 0.0f, 1.0f, 0.0f),
													   vec4f(x + width / 2.0f, y + height / 2.0f, 0.0f, 1.0f));
}

//---------------------------------------------------------------------------
// Attribute functions
//---------------------------------------------------------------------------

void sglColor3f(float r, float g, float b) {
	sglcontrol->currentContext->color.x = r;
	sglcontrol->currentContext->color.y = g;
	sglcontrol->currentContext->color.z = b;
}

void sglAreaMode(sglEAreaMode mode) {
	CHECK_CONTEXTS_DRAWING;
	if (mode == SGL_POINT || mode == SGL_LINE || mode == SGL_FILL) sglcontrol->currentContext->fillMode = mode;
	else setErrCode(SGL_INVALID_OPERATION);
}

void sglPointSize(float size) {
	CHECK_CONTEXTS_DRAWING;
	if (size <= 0) {
		setErrCode(SGL_INVALID_VALUE);
		return;
	}

	sglcontrol->currentContext->ptSize = static_cast<int>(round(size));
}

void sglEnable(sglEEnableFlags cap) {
	CHECK_CONTEXTS_DRAWING;
	if (cap == SGL_DEPTH_TEST) {
		sglcontrol->currentContext->depthTest = true;
	}
	else {
		setErrCode(SGL_INVALID_ENUM);
		return;
	}
}

void sglDisable(sglEEnableFlags cap) {
	CHECK_CONTEXTS_DRAWING;
	if (cap == SGL_DEPTH_TEST) {
		sglcontrol->currentContext->depthTest = false;
	}
	else {
		setErrCode(SGL_INVALID_ENUM);
		return;
	}
}

//---------------------------------------------------------------------------
// RayTracing oriented functions
//---------------------------------------------------------------------------

void sglBeginScene() {
	CHECK_CONTEXTS_DRAWING;

	sglcontrol->currentContext->clearScene();
	sglcontrol->currentContext->sceneDef = true;
}

void sglEndScene() {
	CHECK_CONTEXTS_DRAWING;
	sglcontrol->currentContext->sceneDef = false;
}

void sglSphere(const float x,
			   const float y,
			   const float z,
			   const float radius) {
	if (sglcontrol->activeContexts <= 0 || sglcontrol->currentContext->drawing || !sglcontrol->currentContext->sceneDef) {
		setErrCode(SGL_INVALID_OPERATION);
		return;
	}

	sglcontrol->currentContext->primitives.push_back(new sgl::Sphere(x, y, z, radius, sglcontrol->currentContext->material));
}

void sglMaterial(const float r,
				 const float g,
				 const float b,
				 const float kd,
				 const float ks,
				 const float shine,
				 const float T,
				 const float ior) {
	CHECK_CONTEXTS_DRAWING;

	sgl::Context * context = sglcontrol->currentContext;

	context->material.color.x = r;
	context->material.color.y = g;
	context->material.color.z = b;
	context->material.kd = kd;
	context->material.ks = ks;
	context->material.shine = shine;
	context->material.T = T;
	context->material.ior = ior;
}

void sglPointLight(const float x,
				   const float y,
				   const float z,
				   const float r,
				   const float g,
				   const float b) {
	if (sglcontrol->activeContexts <= 0 || !sglcontrol->currentContext->sceneDef) {
		setErrCode(SGL_INVALID_OPERATION);
		return;
	}
	sglcontrol->currentContext->lights.push_back(new sgl::PointLight(x, y, z, r, g, b));
}

bool isLightShadowed(const vec3f & surfacePos, const sgl::PointLight * light) {
	sgl::Ray ray = sgl::Ray(surfacePos, normalize(light->pos - surfacePos));
	float lightDist = length(dynamic_cast<const sgl::PointLight *>(light)->pos - surfacePos);
	bool found;
	float t;

	for (auto primitive : sglcontrol->currentContext->primitives) {
		found = primitive->intersect(ray, t);
		if (found && t < (lightDist - FLT_EPS) && t > FLT_EPS) return true;
	}

	return false;
}

sgl::Primitive * findNearestPrimitive(const sgl::Ray & ray, float & dist) {
	sgl::Primitive * nearest = nullptr;
	bool found;
	float t;
	dist = FLT_MAX;

	for (auto primitive : sglcontrol->currentContext->primitives) {
		found = primitive->intersect(ray, t);
		if (found && t < dist && t > FLT_EPS) {
			dist = t;
			nearest = primitive;
		}
	}
	return nearest;
}

sgl::AreaLight * findNearestAreaLight(const sgl::Ray & ray, float & dist) {
	sgl::AreaLight * nearest = nullptr;
	bool found;
	float t;
	dist = FLT_MAX;

	for (auto light : sglcontrol->currentContext->lights) {
		if (typeid(*light).hash_code() == typeid(sgl::AreaLight).hash_code()) {
			for (auto triangle : dynamic_cast<sgl::AreaLight *>(light)->triangles) {
				found = triangle->intersect(ray, t);
				if (found && t < dist && t > FLT_EPS) {
					dist = t;
					nearest = dynamic_cast<sgl::AreaLight *>(light);
				}
			}
		}
	}

	return nearest;
}

bool refractRay(const sgl::Ray & ray, vec3f & refractedDir, vec3f normal, const float & ior) {
	float gamma, sqrterm;
	float cosDirN = dot(ray.direction, normal);

	if (cosDirN < 0.0f) {
		// from outside into the inside of object
		gamma = 1.0f / ior;
	}
	else {
		// from the inside to outside of object
		gamma = ior;
		cosDirN = -cosDirN;
		normal = normal * -1.0f;
	}
	sqrterm = 1.0f - gamma * gamma * (1.0f - cosDirN * cosDirN);

	// Check for total internal reflection, do nothing if it applies.
	if (sqrterm > 0.0f) {
		sqrterm = cosDirN * gamma + sqrt(sqrterm);
		refractedDir = normalize(ray.direction * gamma - normal * sqrterm);
		return true;
	}
	else return false;
}

vec3f evalPointLight(const sgl::Primitive * primitive, const vec3f & surfacePos, const vec3f & normal, const sgl::PointLight * light, const sgl::Ray & ray) {
	vec3f color(0.0f);
	
	if (!isLightShadowed(surfacePos, light)) {
		color = light->lightPhong(primitive->material, surfacePos, ray.origin, normal);
		//color = light->lightBlinnPhong(primitive->material, surfacePos, ray.origin, normal);
		//color = light->lightWard(primitive->material, surfacePos, ray.origin, normal);
	}

	return color;
}

vec3f evalLight(const sgl::Primitive * primitive, const vec3f & surfacePos, const vec3f & normal, const sgl::Light * light, const sgl::Ray & ray) {
	vec3f color(0.0f);

	if (typeid(*light).hash_code() == typeid(sgl::PointLight).hash_code()) {
		color = evalPointLight(primitive, surfacePos, normal, dynamic_cast<const sgl::PointLight *>(light), ray);
	}
	else {
		const sgl::AreaLight * areaLight = dynamic_cast<const sgl::AreaLight *>(light);
		
		for (auto triangle : areaLight->triangles) {
			for (int i = 0; i < RAYS_PER_TRIANGLE; i++) {
				float r1 = static_cast<float>(std::rand()) / RAND_MAX;
				float r2 = static_cast<float>(std::rand()) / RAND_MAX;
				float u = 1 - sqrt(r1);
				float v = (1.0f - r2) * sqrt(r1);
				vec3f ab = triangle->b - triangle->a;
				vec3f ac = triangle->c - triangle->a;
				vec3f p = triangle->a + ab * u + ac * v;
				sgl::PointLight pointLight(p, areaLight->color);
	
				sgl::Ray shadowRay = sgl::Ray(surfacePos, normalize(pointLight.pos - surfacePos));
				float A = 0.5f * length(cross(ab, ac));
				float dist = length(pointLight.pos - surfacePos);
				vec3f w = (dot(triangle->n, (shadowRay.direction * -1.0f)) * A)
					    / (RAYS_PER_TRIANGLE * (areaLight->attenuation.x + dist * areaLight->attenuation.y + dist * dist * areaLight->attenuation.z));

				color += evalPointLight(primitive, surfacePos, normal, &pointLight, ray) * w;
			}
		}
	}

	return color;
}

vec3f getBackgroundColor(const sgl::Ray & ray) {
	sgl::Context * context = sglcontrol->currentContext;
	vec3f color;
	
	if (context->environmentMap == nullptr) {
		color = context->clearColor;
	}
	else {
		float c = sqrt(ray.direction.x * ray.direction.x + ray.direction.y * ray.direction.y);
		float r = c > 0.0f ? acos(ray.direction.z) / (2 * M_PI * c) : 0.0f;
		float u = 0.5f + ray.direction.x * r;
		float v = 0.5f + ray.direction.y * r;

		color = context->environmentMap->getTexel(u, v);
	}
	
	return color;
}

vec3f castRay(const sgl::Ray & ray, const int & depth) {
	vec3f color = getBackgroundColor(ray);
	float distPrim, distLight;

	sgl::Primitive * primitive = findNearestPrimitive(ray, distPrim);
	sgl::AreaLight * light = findNearestAreaLight(ray, distLight);

	if (primitive != nullptr) {
		if (distLight <= distPrim) {
			color = light->color;
		}
		else {
			color = 0.0f;
			vec3f surfacePos = ray.origin + ray.direction * distPrim;
			vec3f normal = primitive->getNormal(surfacePos);

			for (auto light : sglcontrol->currentContext->lights) {
				color += evalLight(primitive, surfacePos, normal, light, ray);
			}

			if (depth < MAX_REC_LVL) {
				if (primitive->material.ks > 0.0f) {
					sgl::Ray reflectedRay(surfacePos, normalize(normal * 2 * dot(normal, ray.direction * -1.0f) + ray.direction));
					color += castRay(reflectedRay, depth + 1) * primitive->material.ks;
				}
				if (primitive->material.T > 0.0f) {
					sgl::Ray refractedRay;
					refractedRay.origin = surfacePos;
					bool refracted = refractRay(ray, refractedRay.direction, normal, primitive->material.ior);
					if (refracted) color += castRay(refractedRay, depth + 1) * primitive->material.T;
				}
			}
		}
	}

	return color;
}

void sglRayTraceScene() {
	sgl::Context * context = sglcontrol->currentContext;

	mat4f modelviewInv = context->modelviewStack.top();
	modelviewInv.invert();
	mat4f transformInv = context->viewportMatrix * context->projectionStack.top() * context->modelviewStack.top();
	transformInv.invert();
	
	vec4f camPos = modelviewInv * vec4f(0.0f, 0.0f, 0.0f, 1.0f);
	const vec3f from(vec3f(camPos / camPos.w));

	for (int y = 0; y < context->height; ++y) {
		for (int x = 0; x < context->width; ++x) {			
			vec4f pixel(x, y, -1.0f, 1.0f);
			pixel = transformInv * pixel;
			const vec3f to (vec3f(pixel / pixel.w));
			const sgl::Ray ray(from, normalize(to - from));

			vec3f color = castRay(ray, 0);
			
			int pos = 3 * (x + y * context->width);
			context->colorBuffer[pos + 0] = color.x;
			context->colorBuffer[pos + 1] = color.y;
			context->colorBuffer[pos + 2] = color.z;
		}
	}
}

void sglRasterizeScene() {}

void sglEnvironmentMap(const int width,
					   const int height,
					   float *texels) {
	CHECK_CONTEXTS_DRAWING;

	sgl::Context * context = sglcontrol->currentContext;

	if (context->environmentMap != nullptr) {
		delete context->environmentMap;
	}
	context->environmentMap = new sgl::Texture(width, height, texels);
}

void sglEmissiveMaterial(const float r,
						 const float g,
						 const float b,
						 const float c0,
						 const float c1,
						 const float c2
						 ) {
	CHECK_CONTEXTS_DRAWING;
	sgl::Context * context = sglcontrol->currentContext;

	context->areaLightDef = true;

	context->lights.push_back(new sgl::AreaLight(r, g, b, c0, c1, c2));
	context->currentAreaLight = dynamic_cast<sgl::AreaLight *>(context->lights.back());

}

