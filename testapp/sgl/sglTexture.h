#ifndef __SGL_TEXTURE
#define __SGL_TEXTURE

#include "sglMath.h"

namespace sgl {
	class Texture {
	public:
		Texture(const int & width, const int & height, const float * data);
		~Texture(void);

		vec3f getTexel(const float & u, const float & v) const;

	private:
		float * data;
		int width, height;
	};
}
#endif // !__SGL_TEXTURE
