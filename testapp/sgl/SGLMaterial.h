#ifndef __SGL_MATERIAL_H
#define __SGL_MATERIAL_H

#include "sglMath.h"

namespace sgl {
	struct Material {
		vec3f color;
		float kd, ks, shine, T, ior;
	};
}

#endif // !__SGL_MATERIAL_H