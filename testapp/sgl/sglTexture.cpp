#include "sglTexture.h"

sgl::Texture::Texture(const int & width, const int & height, const float * data) :
	width(width),
	height(height) {
	
	int texSize = width * height * 3;
	this->data = new float[texSize];
	for (int i = 0; i < texSize; i++) {
		this->data[i] = data[i];
	}	
}

sgl::Texture::~Texture(void) {
	delete[] data;
}

vec3f sgl::Texture::getTexel(const float & u, const float & v) const {
	int x = u * width;
	int y = (1 - v) * height;
	int pos = (x + y * width) * 3;
	return vec3f(data[pos], data[pos + 1], data[pos + 2]);
}
