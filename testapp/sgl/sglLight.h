#ifndef __SGL_LIGHT_
#define __SGL_LIGHT_

#include <list>
#include "sglMath.h"
#include "sglPrimitive.h"
#include "sglMaterial.h"

namespace sgl {
	struct Light {
		Light(const float & r, const float & g, const float b) : color(vec3f(r, g, b)) { }
		Light(const vec3f & color) : color(color) { }
		virtual ~Light(void) { }

		vec3f color;
	};

	struct PointLight : public Light {
		PointLight(const float & x, const float & y, const float & z, const float & r, const float & g, const float b)
			: Light(r, g, b), pos(vec3f(x, y, z)) { }
		PointLight(const vec3f & position, const vec3f & color)
			: Light(color), pos(position) { }

		vec3f lightLambert(const Material & material, const vec3f & toLight, const vec3f& normal) const;
		vec3f lightPhong(const Material & material, const vec3f & surfacePos, const vec3f & cameraPos, const vec3f & normal) const;
		vec3f lightBlinnPhong(const Material & material, const vec3f & surfacePos, const vec3f & cameraPos, const vec3f & normal) const;
		vec3f lightWard(const Material & material, const vec3f & surfacePos, const vec3f & cameraPos, const vec3f & normal) const;

		vec3f pos;
	};

	struct AreaLight : public Light {
		AreaLight(const float & r, const float & g, const float & b, const float & c0, const float & c1, const float & c2)
			: Light(r, g, b), attenuation(c0, c1, c2) { }
		AreaLight(const vec3f & color, const vec3f & attenuation)
			: Light(color), attenuation(attenuation) { }
		~AreaLight(void);

		vec3f attenuation;
		std::list<Triangle *> triangles;
	};
}

#endif // !__SGL_LIGHTS_