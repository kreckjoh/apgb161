#ifndef __SGL_CONTEXT_H
#define __SGL_CONTEXT_H

#include <vector>
#include <stack>
#include <list>

#include "sglMath.h"
#include "sgl.h"
#include "sglPrimitive.h"
#include "sglLight.h"
#include "sglMaterial.h"
#include "sglTexture.h"

#define SGL_STACK_MAX_SIZE 1024

namespace sgl {
	struct Context {
		Context(int w, int h);
		~Context(void);

		void clearScene(void);

		int width, height;
		int colorChannels; ///< rgb or rgba
		float * depthBuffer;
		float * colorBuffer;

		sglEMatrixMode matrixMode;
		sglEElementType drawMode;
		sglEAreaMode fillMode;

		bool depthTest, drawing, sceneDef, areaLightDef;

		vec3f clearColor, color;
		int ptSize;

		Material material;

		std::vector<vec4f> vertices;
		std::list<Primitive *> primitives;
		std::list<Light *> lights;

		AreaLight * currentAreaLight;

		Texture * environmentMap;

		mat4f viewportMatrix;
		std::stack<mat4f> projectionStack, modelviewStack;
		mat4f * currentMatrix;
	};
}

#endif // __SGL_CONTEXT_H