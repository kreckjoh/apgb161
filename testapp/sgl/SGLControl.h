#ifndef __SGL_CONTROL_H
#define __SGL_CONTROL_H

#include "sglContext.h"
#include "sglMath.h"
#include "sgl.h"

namespace sgl {
	struct Control {
		Control(void);
		~Control(void);

		const int contextsSize;
		Context ** contexts;
		Context * currentContext;
		int currentContextId;
		int activeContexts; //< number of actual contexts in the contexts array
	};
}

#endif // __SGL_CONTROL_H